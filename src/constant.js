export const CATEGORIES = {
    'all' : 'All',
    'starWars' : 'Star Wars',
    'famousPeople' : 'Famous people',
    'saying' : 'Saying',
    'humor' : 'Humor',
    'motivational' : 'Motivational'
};