import React, {Component, Fragment} from 'react';
import axios from '../../axios-quotes';
import QuoteForm from "../../containers/QuoteForm/QuoteForm";

class Change extends Component {
    state = {
        quote: null
    };

    componentDidMount() {
        const id = this.props.match.params.id;
        axios.get('quotes /' + id + '.json').then(response => {
            this.setState({post: response.data});
        })
    }

    changeQuote = quote => {
        const id = this.props.match.params.id;
        axios.put('quotes/'+ id + '.json', quote).then(() => {
            this.props.history.replace('/');
        })
    };
    render() {
        let edit = <QuoteForm
            onSubmit={this.changeQuote}
            quote={this.state.quote}
        />;

        return (
            <Fragment>
                <h1>Edit a quote</h1>
                {edit}
            </Fragment>
        );
    }
}

export default Change;