import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://control-js-3.firebaseio.com/'
});

export default instance;