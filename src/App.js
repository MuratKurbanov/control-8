import React, {Component, Fragment} from 'react';
import Quotes from './containers/Quotes/Quotes';
import SubmitNewQuotes from './containers/SubmitNewQuote/SubmitNewQuote';
import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import {Route, Switch, NavLink as RouterNavLink} from "react-router-dom";
import Change from "./components/Change/Change";

class App extends Component {
  render() {
    return (
        <Fragment>
          <Navbar dark color="success" light expand="md">
            <NavbarBrand>Quotes Central</NavbarBrand>
            <NavbarToggler />
            <Collapse isOpen navbar>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <NavLink tag={RouterNavLink} to="/" exact>Quotes</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink tag={RouterNavLink} to="/Submit">Submit new quote</NavLink>
                </NavItem>
              </Nav>
            </Collapse>
          </Navbar>
          <Container>
            <Switch>
              <Route path="/" exact component={Quotes}/>
              <Route path="/Submit" exact component={SubmitNewQuotes}/>
              <Route path="/posts/:id/edit" component={Change}/>
              <Route render={() => <h1>Not found</h1>}/>
            </Switch>
          </Container>
        </Fragment>
    );
  }
}

export default App;
