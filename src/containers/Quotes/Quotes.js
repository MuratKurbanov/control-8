import React, {Component} from 'react';
import axios from '../../axios-quotes';
import {NavLink} from "react-router-dom";
import './Quotes.css';

class Quotes extends Component {
    state = {
        quotes: null
    };

    componentDidMount() {
        axios.get('quotes.json').then(response => {
            const quotes = Object.keys(response.data).map(id => {
                return {...response.data[id], id};
            });

            this.setState({quotes})
        })
    }

    deleteQuote = (id) => {
        axios.delete('quotes/' + id + '.json').then(() => {
            this.props.history.push('/')
        })
    };

    render() {
        let quotes = null;

        if (this.state.quotes) {
            quotes = this.state.quotes.map((quote, index) => (
                 <div key={index} className='post'>
                    <p>{quote.quoteText}</p>
                    <p>- {quote.author}</p>
                    <NavLink to={"/posts/" + quote.id + '/edit'}><button>Edit</button></NavLink>
                    <button onClick={() => this.deleteQuote(quote.id)}>Delete</button>
                </div>
            ));
        }
        return (
            <div className='quotes'>
                <div className='link'>
                    <ul>
                        <li><a href='#'>All</a></li>
                        <li><a href="#">Star Wars</a></li>
                        <li><a href="#">Famous people</a></li>
                        <li><a href="#">Saying</a></li>
                        <li><a href="#">Humor</a></li>
                        <li><a href="#">Motivational</a></li>
                    </ul>
                </div>
                <div>
                    {quotes}
                </div>
            </div>
        );
    }
}

export default Quotes;