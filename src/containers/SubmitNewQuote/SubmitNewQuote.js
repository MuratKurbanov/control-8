import React, {Component, Fragment} from 'react';
import axios from '../../axios-quotes';
import QuoteForm from "../../containers/QuoteForm/QuoteForm";

class SubmitNewQuote extends Component {
    addQuote = quote => {
        axios.post('quotes.json', quote).then(() => {
            this.props.history.replace('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>Submit new quote</h2>
                <QuoteForm onSubmit={this.addQuote}/>
            </Fragment>
        );
    }
}

export default SubmitNewQuote;