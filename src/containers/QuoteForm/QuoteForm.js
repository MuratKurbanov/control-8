import React, {Component} from 'react';
import {CATEGORIES} from "../../constant";

class QuoteForm extends Component {
    constructor(props) {
        super(props);

        if (props.quote) {
            this.state = {
                category: this.props.quote.category,
                author: this.props.quote.author,
                quoteText: this.props.quote.quoteText
            }
        } else {
            this.state = {
                category: Object.keys(CATEGORIES)[0],
                author: '',
                quoteText: ''
            };
        }
    }

    changeHandler = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    submitHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    render() {
        return (
            <form onSubmit={this.submitHandler}>
                <select name="category" onChange={this.changeHandler} value={this.state.category}>
                    {Object.keys(CATEGORIES).map(categoryId =>(
                        <option value={categoryId}>{CATEGORIES[categoryId]}</option>
                    ))}
                </select>
                <div>
                    <p>Author</p>
                    <input type="text" name='author'
                           value={this.state.author} onChange={this.changeHandler}
                    />
                </div>
                <div>
                    <p>Quote Text</p>
                    <input type="text" name='quoteText'
                           value={this.state.quoteText} onChange={this.changeHandler}
                    />
                </div>
                <div>
                    <button type='submit'>Save</button>
                </div>
            </form>
        );
    }
}

export default QuoteForm;